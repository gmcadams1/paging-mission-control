/**
 * File: Driver.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.json.JSONArray;

/**
 * Class that allows configuration/running of the application.
 * 
 * @author gmcadams1
 *
 */
public class Driver {
	
	// Configuration
	private static final Integer INTERVAL_IN_MINUTES = 5;
	private static final Integer MAX_LOGS_PER_INTERVAL = 3;
	private static final String DELIMITER = "\\|";
	// Each Satellite we see
	private List<Satellite> sats;
	// Each message we process
	private List<Reading> reads;
	private static Logger logger = Logger.getLogger(Driver.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    Driver driver = new Driver();
	    
	    if (args.length == 0) {
	        driver.run("resources\\Input1.txt");
	    }
	    else if (args.length > 1) {
	        System.out.println("Usage: <input_path.txt>");
	    } else {
	        driver.run(args[0]);
	    }
	}
	
	/**
     * Default Driver constructor
     * 
     */
	public Driver() {
		this.sats = new ArrayList<Satellite>();
		this.reads = new LinkedList<Reading>();
	}
	
	/**
     * Run the program with the given input file.
     * 
     * @param inputFile The input file of sensor readings
     */
	public void run(String inputFile) {
		// Read inputs and create Readings for each line
        Path path = Paths.get(inputFile);
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(line -> reads.add(new Reading(line)));
        } catch (IOException ex) {
        	logger.error("IOException in input file: " + ex.getMessage());
        }
        
        // Process each reading
        process();
        // Generate output
        output();
	}
	
	/**
     * Do the main bulk processing of the application.
     * 
     * Go through each line and process the data.
     * 
     */
	private void process() {
		// Date format for timestamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		
        // Go through each Reading parsed from input file
        for (Reading r : this.reads) {
        	try {
        		// Parse the reading given a delimiter and timestamp Date format
				r.parseReading(DELIMITER, dateFormat);
				
				// Get satellite id for this Reading    	
	        	Integer satId = r.getSatellite();
	        	
	        	// If the satellite is new, add the new satellite
	        	if (!this.sats.stream().anyMatch(sat -> sat.getId().equals(satId))) {
	        		this.sats.add(new Satellite(satId));
	        	}
	        	
	        	// Find the Satellite and process next reading
	        	// Note: If reading has an invalid component here, 
	        	//		 a new Satellite will still have been created
	        	for (Satellite s : this.sats) {
	        		if (s.getId().equals(r.getSatellite())) {
	        			s.nextReading(r);
	        			break;
	        		}
	        	}
			} catch (ParseException e) {
				logger.warn("Error parsing timestamp for " + r.toString());
				continue;
			} catch (InvalidComponentException e) {
				logger.warn("Invalid component name " + r.getComponent());
			}
        }
	}
	
	/**
     * Generate the output after all data has been loaded/processed.
     * 
     */
	private void output() {
		// Output JSON array
		JSONArray output = new JSONArray();
		
        // Go through flagged logs for each Satellite
        for (Satellite s : this.sats) {
        	logger.debug("Sat: " + s);
        	s.getAllFlaggedLogs(INTERVAL_IN_MINUTES, MAX_LOGS_PER_INTERVAL)
        		.stream().forEach(x -> output.put(x));
        }
        
        // Output our final flagged logs from every Satellite
        System.out.println(output.toString(3));
	}
}
