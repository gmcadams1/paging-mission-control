/**
 * File: ReadingTest.java
 * Date: July 18, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/

package com.enlighten.pmc.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.enlighten.pmc.Reading;

class ReadingTest {

	String delimiter = "\\|";
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	
	@Test
	void readingTestGood() {
		Reading read = new Reading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		Assertions.assertDoesNotThrow(() -> {
			read.parseReading(delimiter, dateFormat);
		});
	}

	@Test
	void readingTestBad() {
		Reading read = new Reading("20180101A 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		Assertions.assertThrows(ParseException.class, () -> {
			read.parseReading(delimiter, dateFormat);
		});
	}
}
