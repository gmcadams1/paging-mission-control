/**
 * File: Thermostat.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Class representing a Thermostat Component.
 * 
 * @author gmcadams1
 *
 */
public class Thermostat extends Component {
		
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Thermostat constructor with a given unique name
	 * 
	 * @param name The unique name of this Thermostat
	 * 
	 */
	public Thermostat(String name) {
		super(name);
	}
	
	/**
	 * @return string representation of a Thermostat
	 * 
	 */
	@Override
	public String toString() {
		return "TSTAT-" + super.getName();
	}

	/**
	 * Retrieve logs based on a particular "bad" state of a Thermostat.
	 * 
	 * @return the log entries in which a Thermostat is in a flagged state.
	 * 
	 */
	@Override
	public List<AbstractMap.SimpleEntry<Reading, Component.State>> getFlaggedLogs() {
		List<AbstractMap.SimpleEntry<Reading, Component.State>> out 
			= new LinkedList<AbstractMap.SimpleEntry<Reading, Component.State>>();
		
		for (AbstractMap.SimpleEntry<Reading, Component.State> log : super.getLog()) {
			if (log.getValue() == Component.State.RED_HIGH) {
				out.add(log);
			}
		}
		
		return out;
	}
}
