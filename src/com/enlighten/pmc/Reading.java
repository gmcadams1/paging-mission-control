/**
 * File: Reading.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Class representing a single sensor reading.
 * 
 * @author gmcadams1
 *
 */
public class Reading {
	// Raw input string from source input file
	private String rawReading;
	// <timestamp> component of a Reading
	private Date timestamp;
	// <component> component of a Reading
	private String component;
	// <raw-value> component of a Reading
	private Double value;
	// red/yellow limit components of a Reading
	private List<Double> params;
	// <satellite-id> component of a Reading
	private Integer sat;
	private static Logger logger = Logger.getLogger(Reading.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		
		Reading test = new Reading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
		try {
			test.parseReading("\\|", dateFormat);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Reading constructor for next sensor reading
	 * 
	 * @param rawReading The full input line of a reading
	 * 
	 */
	public Reading(String rawReading) {
		this.rawReading = rawReading;
		this.params = new ArrayList<Double>();
	}
	
	/**
	 * Reading copy constructor
	 * 
	 * @param copy The Reading to copy
	 * 
	 */
	public Reading(Reading copy) {
		this(copy.rawReading);
		this.timestamp = new Date(copy.timestamp.getTime());
		this.component = copy.component;
		this.value = copy.value;
		this.params = new ArrayList<Double>(copy.params);
		this.sat = copy.sat;
	}
	
	/**
	 * Parse the reading by delimiter and deconstruct into its parts
	 * 
	 * @param delimiter  The delimiter to split the values
	 * @param dateFormat The format of the timestamp component
	 * @throws ParseException
	 * 
	 */
	public void parseReading(String delimiter, SimpleDateFormat dateFormat)
			throws ParseException {		
		String[] parts = rawReading.split(delimiter);
		
		try {
			this.timestamp = dateFormat.parse(parts[0]);
		} catch (ParseException e) {
			logger.error("Error parsing date");
			throw e;
		}
		this.sat = Integer.valueOf(parts[1]);
		this.component = parts[parts.length-1];
		this.value = Double.valueOf(parts[parts.length-2]);
		
		// Parse all red/yellow low/high parts
		for (int i = 2; i < parts.length-2; i++) {
			this.params.add(Double.valueOf(parts[i]));
		}
	}
	
	/**
	 * @return the id of the Satellite that this Reading belongs to
	 * 
	 */
	public Integer getSatellite() {
		return this.sat;
	}
	
	/** 
	 * @return the Component name of the Satellite that this reading belongs to
	 * 
	 */
	public String getComponent() {
		return this.component;
	}
	
	/**
	 * Get the red or yellow, high or low values of the reading
	 * 
	 * @param index The index to get
	 * @return the value to get
	 * 
	 */
	public Double getParam(Integer index) {
		return this.params.get(index);
	}
	
	/**
	 * @return the timestamp value to get as a Date
	 * 
	 */
	public Date getTimestamp() {
		return this.timestamp;
	}
	
	/**
	 * @param newTime The new timestamp
	 * 
	 */
	public void setTime(Date newTime) {
		this.timestamp = new Date(newTime.getTime());
	}
	
	/**
	 * @return the raw value component of the reading
	 * 
	 */
	public Double getValue() {
		return this.value;
	}
	
	/**
	 * @return string representation of a Reading
	 * 
	 */
	@Override
	public String toString() {
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return outputFormat.format(this.timestamp) + " " + this.sat + " " 
				+ this.component + " " + this.params + " " + this.value;
	}
}
