/**
 * File: InvalidComponentException.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

/**
 * Thrown if a Component specified in a Reading is invalid.
 * 
 * @author gmcadams1
 *
 */
public class InvalidComponentException extends Exception {

	/**
	 * Generated
	 */
	private static final long serialVersionUID = -5091207101577953636L;

	public InvalidComponentException(String string) {
		super(string);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
