/**
 * File: Satellite.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * Class representing a satellite equipped
 * with components that receive sensor data.
 * Uniquely identified by an integer id.
 * 
 */
public class Satellite {
	
	private Integer id;
	private List<Component> components;
	private static Logger logger = Logger.getLogger(Satellite.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		// TODO Auto-generated method stub
		Satellite test = new Satellite(1);
		test.addComponent(new Thermostat("TSTAT"));
		try {
			Reading r = new Reading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
			r.parseReading("\\|", dateFormat);
			test.nextReading(r);
		} catch (InvalidComponentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Satellite constructor with a given unique id
	 * 
	 * @param id The unique id of this Satellite
	 * 
	 */
	public Satellite(Integer id) {
		this.id = id;
		this.components = new ArrayList<Component>();
	}
	
	/**
	 * Satellite constructor with a given unique id
	 * 
	 * @param comp A component to add
	 * 
	 */
	public void addComponent(Component comp) {
		this.components.add(comp);
	}
	
	/**
	 * A new reading from one component on the Satellite
	 * 
	 * @param msg The new Reading from a sensor
	 * @throws InvalidComponentException if the component is invalid
	 */
	public void nextReading(Reading msg) throws InvalidComponentException {
		// Add component if it doesn't exist
		if (!this.components.stream().anyMatch(c -> c.getName().equals(msg.getComponent()))) {
			if (msg.getComponent().contains("TSTAT"))
				addComponent(new Thermostat(msg.getComponent()));
			else if (msg.getComponent().contains("BATT"))
				addComponent(new Battery(msg.getComponent()));
			else
				throw new InvalidComponentException("Invalid component found: " + msg.getComponent());
		}
		
		// Update component with new reading
		this.updateComponent(msg);
	}
	
	/**
	 * Find which component this message belongs to 
	 * and update its state accordingly.
	 * 
	 * @param msg The Reading from a sensor
	 * @throws InvalidComponentException when Reading component name does not match Component name
	 */
	private void updateComponent(Reading msg) throws InvalidComponentException {
		// Update state of this component
		for (Component c : this.components) {
			if (c.getName().equals(msg.getComponent())) {
				try {
					c.updateComponent(msg);
				} catch (InvalidComponentException e) {
					logger.error("Updated incorrect component " + e.getMessage());
					throw e;
				}
				break;
			}
		}
	}
	
	/**
	 * @return this Satellite's unique id
	 * 
	 */
	public Integer getId() {
		return this.id;
	}
	
	/**
	 * Get flagged logs for each component on the Satellite.
	 * 
	 * @param interval The interval period (in minutes)
	 * @param max The maximum number of flagged readings allowed in an interval
	 * @return the flagged logs as a list of JSONObjects
	 * 
	 */
	public List<JSONObject> getAllFlaggedLogs(Integer interval, Integer max) {
		List<JSONObject> flaggedLog = new ArrayList<JSONObject>();
		
		// Need to go through each component separately
		logger.debug("Size of components: " + this.components.size());
		for (Component c : this.components) {
			logger.debug("Component: " + c);
			Integer badCount = 0;
			Date window = null;
			// For each log in a Component
			for (AbstractMap.SimpleEntry<Reading, Component.State> log : c.getFlaggedLogs()) {
				Reading r = log.getKey();
				// If this is first flagged log or if the window has surpassed the interval
				if (window == null
						|| Duration.between(window.toInstant(), r.getTimestamp().toInstant()).toMinutes() > interval) {
					window = r.getTimestamp();
					badCount = 1;
					continue;
				} else { // Else, we are still within the given interval
					badCount++;
					
					// Only report the max flagged log in each given interval
					if (badCount == max) {
						flaggedLog.add(getFlaggedEntry(log.getValue(), r));
					}	
				}
			}
		}
		
		return flaggedLog;
	}
	
	/**
	 * Build flagged log entry for output.
	 * 
	 * @param s The Component's state
	 * @param r The log Reading
	 * @return the flagged log as a JSONObject
	 * 
	 */
	private JSONObject getFlaggedEntry(Component.State s, Reading r) {
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

		JSONObject obj = new JSONObject();
		obj.put("satelliteId", this.id);
		obj.put("severity", s.toString());
		obj.put("component", r.getComponent());
		obj.put("timestamp", outputFormat.format(r.getTimestamp()));
		
		return obj;
	}
	
	/**
	 * @return string representation of Satellite
	 * 
	 */
	@Override
	public String toString() {
		return Integer.toString(this.id);
	}
}
