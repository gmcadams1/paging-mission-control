/**
 * File: Component.java
 * Date: July 13, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/
package com.enlighten.pmc;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Class representing a Component with a single sensor that has defined states.
 * 
 * @author gmcadams1
 *
 */
public abstract class Component {
	/**
	 * Enum representing a finite set of valid states for a component.
	 * Given an integer value in order to reference index of input data.
	 * 
	 */
	enum State {
		RED_HIGH(0), 
		YELLOW_HIGH(1), 
		YELLOW_LOW(2), 
		RED_LOW(3),
		NEUTRAL(4);
		
		int index;
		
		/**
		 * Satellite constructor with a given unique id
		 * 
		 * @param index The index of this State
		 * 
		 */
		State(int index) {
			this.index = index;
		}
		
		@Override
		public String toString() {
			switch(this) {
				case NEUTRAL: return "NEUTRAL";
				case RED_HIGH: return "RED HIGH";
				case YELLOW_HIGH: return "YELLOW HIGH";
				case RED_LOW: return "RED LOW";
				case YELLOW_LOW: return "YELLOW LOW";
				default: throw new IllegalArgumentException();
			}
		}
	}
	
	// Unique name of this Component
	private String name;
	// Current state of the Component
	private State state;
	// Current sensor value of the component
	private Double value;
	// Log of all previous value/state pairs (i.e. all Readings)
	private List<AbstractMap.SimpleEntry<Reading, Component.State>> log;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Component constructor with a given unique name
	 * 
	 * @param name The unique name of this Component
	 * 
	 */
	public Component(String name) {
		this.name = name;
		this.state = State.NEUTRAL;
		this.log = new LinkedList<AbstractMap.SimpleEntry<Reading, Component.State>>();
	}
	
	/**
	 * @param value The new value of this Component
	 * 
	 */
	public void setValue(Double value) {
		this.value = value;
	}
	
	/**
	 * @return the current value of this Component
	 * 
	 */
	public Double getValue() {
		return this.value;
	}
	
	/**
	 * @return the current state of this Component
	 * 
	 */
	public State getState() {
		return this.state;
	}
	
	/**
	 * @param state The new state of this Component
	 * 
	 */
	public void setState(State state) {
		this.state = state;
	}
	
	/**
	 * Updates the component state based on the incoming sensor reading.
	 * Also adds to the on-going Component log history.
	 * 
	 * @param msg The most recent sensor message
	 * @throws InvalidComponentException when Reading component name does not match Component name
	 */
	public void updateComponent(Reading msg) throws InvalidComponentException {
		if (!msg.getComponent().equals(this.name)) {
			throw new InvalidComponentException("Wrong component: " + msg.getComponent());
		}
		
		this.setValue(msg.getValue());
		
		if (this.getValue() > msg.getParam(Component.State.RED_HIGH.index)) {
			this.setState(Component.State.RED_HIGH);
		} else if (this.getValue() > msg.getParam(Component.State.YELLOW_HIGH.index)) {
			this.setState(Component.State.YELLOW_HIGH);
		} else if (this.getValue() < msg.getParam(Component.State.RED_LOW.index)) {
			this.setState(Component.State.RED_LOW);
		} else if (this.getValue() < msg.getParam(Component.State.YELLOW_LOW.index)) {
			this.setState(Component.State.YELLOW_LOW);
		} else {
			this.setState(Component.State.NEUTRAL);
		}
		
		log.add(new AbstractMap.SimpleEntry<Reading, Component.State>(msg, this.getState()));
	}
	
	/**
	 * @return the unique name of this Component
	 * 
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return the entire log of values/states seen
	 * 
	 */
	public List<AbstractMap.SimpleEntry<Reading, Component.State>> getLog() {
		return this.log;
	}
	
	/**
	 * Retrieve logs based on a particular "bad" state implementation of a component.
	 * 
	 * @return the log entries in which a Component is in a flagged state.
	 * 
	 */
	public abstract List<AbstractMap.SimpleEntry<Reading, Component.State>> getFlaggedLogs();
}
