/**
 * File: ComponentTest.java
 * Date: July 18, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/

package com.enlighten.pmc.test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.enlighten.pmc.Battery;
import com.enlighten.pmc.Component;
import com.enlighten.pmc.InvalidComponentException;
import com.enlighten.pmc.Reading;
import com.enlighten.pmc.Thermostat;

class ComponentTest {

	String delimiter = "\\|";
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	
	@Test
	void thermostatTest() {
		Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT");
		try {
			one.parseReading(delimiter, dateFormat);
		} catch (ParseException e1) {
			e1.printStackTrace();
			Assert.fail();
		}
		Component ts = new Thermostat("TSTAT");
		try {
			ts.updateComponent(one);
		} catch (InvalidComponentException e) {
			e.printStackTrace();
			Assert.fail();
		}
		assertEquals(ts.getFlaggedLogs().size(), 1);
	}

	@Test
	void batteryTest() {
		Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|102|BATT");
		try {
			one.parseReading(delimiter, dateFormat);
		} catch (ParseException e1) {
			e1.printStackTrace();
			Assert.fail();
		}
		Component bat = new Battery("BATT");
		try {
			bat.updateComponent(one);
		} catch (InvalidComponentException e) {
			e.printStackTrace();
			Assert.fail();
		}
		assertEquals(bat.getFlaggedLogs().size(), 0);
	}
	
	@Test
	void invalidReading() {
		Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT");
		Component bat = new Battery("BATT");
		try {
			one.parseReading(delimiter, dateFormat);
		} catch (ParseException e1) {
			e1.printStackTrace();
			Assert.fail();
		}
		Assertions.assertThrows(InvalidComponentException.class, () -> {
			bat.updateComponent(one);
		});
	}
}
