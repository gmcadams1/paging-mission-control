/**
 * File: SatelliteTest.java
 * Date: July 18, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/

package com.enlighten.pmc.test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import com.enlighten.pmc.InvalidComponentException;
import com.enlighten.pmc.Reading;
import com.enlighten.pmc.Satellite;

class SatelliteTest {
	
	String delimiter = "\\|";
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

	@Test
	void noComponents() {
		Satellite test = new Satellite(1);
		
		List<JSONObject> out = test.getAllFlaggedLogs(5,3);
		assertEquals(out.size(), 0);
	}
	
	@Test
	void noFlaggedLogs() {
		Satellite test = new Satellite(1001);

		try {
			Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT");
			one.parseReading(delimiter, dateFormat);
			test.nextReading(one);
		} catch (InvalidComponentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
		List<JSONObject> out = test.getAllFlaggedLogs(5,3);
		
		assertEquals(out.size(), 0);
	}

	@Test
	void oneFlaggedIn5() {
		Satellite test = new Satellite(1001);

		try {
			Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT");
			Reading two = new Reading("20180101 23:02:05.001|1001|101|98|25|20|103|TSTAT");
			Reading three = new Reading("20180101 23:03:05.001|1001|101|98|25|20|104|TSTAT");
			one.parseReading(delimiter, dateFormat);
			two.parseReading(delimiter, dateFormat);
			three.parseReading(delimiter, dateFormat);
			test.nextReading(one);
			test.nextReading(two);
			test.nextReading(three);
		} catch (InvalidComponentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
		List<JSONObject> out = test.getAllFlaggedLogs(5,3);
		
		assertEquals(out.size(), 1);
	}
	
	@Test
	void manyFlaggedIn5() {
		Satellite test = new Satellite(1001);

		try {
			Reading one = new Reading("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT");
			one.parseReading(delimiter, dateFormat);
			
			// Generate 100 flagged logs within 5 minutes
			for (int i = 0; i < 100; i++) {
				// Create a copy and increase the time slightly
				Reading next = new Reading(one);
				next.setTime(new Date(one.getTimestamp().getTime()+(i*100)));
				test.nextReading(next);
			}
			
		} catch (InvalidComponentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
		List<JSONObject> out = test.getAllFlaggedLogs(5,3);
		
		assertEquals(out.size(), 1);
	}
}
