/**
 * File: DriverTest.java
 * Date: July 18, 2020
 * E-mail: gmcadams1@comcast.net
 * 
 * @author  gmcadams1
 * @version 1.0
 **/

package com.enlighten.pmc.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import com.enlighten.pmc.Driver;

class DriverTest {

	@Test
	void input1() {
		Driver driver = new Driver();
		System.out.println("Test 1:");
		try {
			driver.run("resources\\Input1.txt");
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	void input2() {
		Driver driver = new Driver();
		System.out.println("Test 2:");
		try {
			driver.run("resources\\Input2.txt");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	void input3() {
		Driver driver = new Driver();
		System.out.println("Test 3:");
		try {
			driver.run("resources\\Input3.txt");
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
